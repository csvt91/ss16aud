package genList;

/**
 * Created by User on 28.04.2016.
 */

public class damain {
    public static void main(String args[]) {
        GenList<NatNum> lst = GenList.cons(
                NatNum.toNatNum(3), GenList.cons(
                        NatNum.toNatNum(2), GenList.cons(
                                NatNum.toNatNum(5), GenList.cons(
                                        NatNum.toNatNum(4), GenList.cons(
                                                NatNum.toNatNum(1), GenList.nil())))));
        selectionSort(lst);
        System.out.println(lst);
    }

    private static void aufgabe1() {
        NatNum x = new Next(new Next(new Null()));
        NatNum y = new Next((new Next(new Next(new Next(new Next(new Null()))))));
        System.out.println(x + " + " + y + " = " + NatNum.add(x, y));
        System.out.println(x + " * " + y + " = " + NatNum.mul(x, y));
    }

    private static void aufgabe1_2() {
        NatNum x = NatNum.toNatNum(2);
        NatNum y = NatNum.toNatNum(4);
        System.out.println(x + " + " + y + " = " + NatNum.add(x, y));
        System.out.println(x + " * " + y + " = " + NatNum.mul(x, y));
    }

    private static void aufgabe2() {
        GenList<NatNum> lst = GenList.cons(
                NatNum.toNatNum(1), GenList.cons(
                        NatNum.toNatNum(2), GenList.cons(
                                NatNum.toNatNum(3), GenList.nil())));
        System.out.println(lst);
    }

    static <T extends Comparable<T>> void selectionSort(GenList<T> lst) {
        GenList<T> i = lst;
        while (!(i instanceof Nil)) {
            // lst up to element before i is sorted into result
            Cons<T> lst_i = (Cons<T>) i;
            Cons<T> small = lst_i;
            GenList<T> j = lst_i.tail;
            while (!(j instanceof Nil)) {
                Cons<T> lst_j = (Cons<T>) j;
                if (lst_j.head.compareTo(small.head) < 0) {
                    small = lst_j;
                }
                j = lst_j.tail;
            }
            // small is smallest in lst starting with i
            // swap lst_i and a[small]
            T t = small.head;
            small.head = lst_i.head;
            lst_i.head = t;
            i = lst_i.tail;
        }
    }
}
