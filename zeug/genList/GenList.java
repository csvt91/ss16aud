package genList;

/**
 * Created by User on 28.04.2016.
 */
interface GenList<T> {
    static <T> GenList<T> cons(T head, GenList<T> tail) {
        return new Cons<T>(head, tail);
    }

    static <T> GenList<T> nil() {
        return new Nil<>();
    }

}

class Nil<T> implements GenList<T> {
    @Override
    public String toString() {
        return "Nil []";
    }
}

class Cons<T> implements GenList<T> {
    T head;
    GenList<T> tail;

    Cons(T head, GenList<T> tail) {
        this.head = head;
        this.tail = tail;
    }

    @Override
    public String toString() {
        return "Cons [head=" + head + ", tail=" + tail + "]";
    }
}