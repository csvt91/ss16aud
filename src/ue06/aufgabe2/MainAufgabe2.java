package ue06.aufgabe2;

import carofuns.Funk;

/**
 * @author Caroline Vetter (977447)
 * @version 0 @21.05.2016.
 */
public class MainAufgabe2 {
    public static void main(String args[]) {
        Funk ary = new Funk(new Integer[]{89, 45, 68, 90, 29, 34, 17});
        /*
         * Aufgabe 2.1
         */
        Integer[] arr = (Integer[]) ary.bubbleSort();
        System.out.println(ary.ArrayToString());
        /*
         * Aufgabe 2.2
         * Stimmt es, dass die äußere Schleife von Bubble–Sort (und damit der Sortierlauf insgesamt) beendet werden kann, wenn
         * der inneren Schleife keine swap –Operation ausgeführt wurde?
         *
         * Ja, es stimmt, es wird halt dann nichts sortiert.
         */
    }

}
