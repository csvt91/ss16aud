package ue06.aufgabe6;


import ue06.aufgabe6.model.Model;
import ue06.aufgabe6.view.BoardView;

import java.util.NoSuchElementException;

public class Control {

    private Model model;
    private BoardView view;

    public Control(int n) {
    }

    public void setView(BoardView view) {
        this.view = view;
    }

    public void setModel(Model model) {
        this.model = model;
    }


    public void next() {
        try {
            model.next();
        } catch (NoSuchElementException e) {
            view.setMsg("Finished");
        }
    }
}
