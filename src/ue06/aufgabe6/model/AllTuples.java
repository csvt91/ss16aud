package ue06.aufgabe6.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

class AllTuples {

    private Set<Integer> M = null;

    /**
     * @param M Set of elements that form the tuples.
     */
    public AllTuples(Set<Integer> M) {
        this.M = M;
    }

    /**
     * Build all tuples of size k build from elements of a set M.
     *
     * @param k size tuples.
     * @return List of all tuples of size k build from elements of a set M
     */
    public List<Integer[]> ofLength(int k) {
        if (k == 0) {
            ArrayList<Integer[]> result = new ArrayList<>();
            result.add((Integer[]) new Integer[0]);
            return result;
        } else {
            List<Integer[]> result = new ArrayList<>();
            List<Integer[]> rec = ofLength(k - 1);
            for (Integer[] t : rec) {
                for (int x : M) {
                    Integer[] tt = Arrays.copyOf(t, t.length + 1);
                    tt[tt.length - 1] = x;
                    result.add(tt);
                }
            }
            return result;
        }
    }
}

class QueensProblem {
    /**
     * @author: Caroline Vetter, 977447
     */
    List<Integer[]> savedBoards;
    int queensN;

    QueensProblem(int queens) {
        this.queensN = queens;
        this.savedBoards = new ArrayList<>();
        putQueensInRow(new Integer[queens], 0);
    }

    /**
     * Checks the board if a queen is already in the row or in the diagonal (both)
     *
     * @param board current Board of Queens
     * @param row   current row
     * @return true if the queen has no conflicts
     */
    private boolean checkQueen(Integer[] board, int row) {
        for (int i = 0; i < row; i++) {
            if (board[i] == board[row]) return false;
            if ((board[i] - board[row]) == (row - i)) return false;
            if ((board[row] - board[i]) == (row - i)) return false;
        }
        return true;
    }

    /**
     * Builds up the Board. If the checks in checkQueen are successful, the Queen is passed to save.
     * RECURSIVE.
     *
     * @param board current Board of Queens
     * @param row   current row
     */
    private void putQueensInRow(Integer[] board, int row) {
        if (row == this.queensN) savedBoards.add(saveQueen(board));
        else {
            for (int i = 0; i < this.queensN; i++) {
                board[row] = i;
                if (checkQueen(board, row)) putQueensInRow(board, row + 1);
            }
        }
    }

    /**
     * Saves the Queens Positions and returns them in an Array
     *
     * @param board current board of Queens
     * @return Queens Positions on the Board in a field
     */
    private Integer[] saveQueen(Integer[] board) {
        Integer[] savedQueens = new Integer[queensN];
        for (int i = 0; i < this.queensN; i++) {
            for (int j = 0; j < this.queensN; j++) {
                if (board[i] == j) savedQueens[i] = j;
            }
        }
        return savedQueens;
    }

    /**
     * TODO
     *
     * @return all Boards
     */
    List<Integer[]> getBoards() {
        return savedBoards;
    }
}