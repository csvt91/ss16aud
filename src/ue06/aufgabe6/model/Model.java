package ue06.aufgabe6.model;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import ue06.aufgabe6.Position;
import ue06.aufgabe6.view.BoardView;

import java.util.*;

public class Model {

    private final int n;

    private Map<Position, Boolean> board;
    private ObservableMap<Position, Boolean> observableBoard;

    private List<Integer[]> placements;
    private Iterator<Integer[]> placementIter;

    public Model(int n, final BoardView view) {
        this.n = n;
        board = new TreeMap<Position, Boolean>();
        observableBoard = FXCollections.observableMap(board);
        QueensProblem qn = new QueensProblem(n);
        Set<Integer> positions = new TreeSet<>();
        for (int i = 0; i < n; i++)
            positions.add(i);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                observableBoard.put(new Position(i, j), false);
            }
        }
        Thread allQueensThread = new Thread(() -> {
            placements = qn.getBoards();
            placementIter = placements.iterator();
            Platform.runLater(
                    () -> {
                        view.setMsg("");
                    }
            );
        });
        view.setMsg("Computing, please wait!");
        allQueensThread.start();
    }

    public void addObserver(MapChangeListener<Position, Boolean> boardObserver) {
        observableBoard.addListener(boardObserver);
    }

    public void next() {
        if (placementIter.hasNext()) {
            Integer[] nextPlacement = placementIter.next();
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    observableBoard.put(new Position(i, j), false);
                }
            }
            int x = 0;
            for (int y : nextPlacement) {
                observableBoard.put(new Position(x, y), true);
                x = x + 1;
            }
        } else {
            throw new NoSuchElementException();
        }
    }

}
