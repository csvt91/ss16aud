package ue07.aufgabe6;

import java.util.AbstractQueue;
import java.util.Iterator;

public class HeapPriorityQueue<E extends Comparable<E>> extends AbstractQueue<E> {
    private int capacity;
    private int size = 0;
    private Iterable<? extends Object> queue;

    /**
     * Erzeugt eine
     * Prioritaetswarteschlange.
     *
     * @param capacity die Kapazitaet der Warteschlange.
     * @throws IllegalArgumentException falls capacity<=0
     */
    public HeapPriorityQueue(int capacity) throws IllegalArgumentException {
        if (capacity <= 0) {
            throw new IllegalArgumentException();
        }
        this.capacity = capacity;
        // TODO Initialisierung Ihrer Datenstruktur

    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean offer(E e) {
        return false;
    }

    @Override
    public E poll() {
        return null;
    }

    @Override
    public E peek() {
        return null;
    }

    public Iterable<? extends Object> getQueue() {
        return queue;
    }
}