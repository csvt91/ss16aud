package ue07.aufgabe7_8;

import javafx.scene.control.TreeView;

public class Control {
    private HeapPriorityQueue<Integer> model;
    private ConsoleView view;

    public Control(HeapPriorityQueue<Integer> model) {
        this.model = model;
    }

    public void insert(int n) throws IndexOutOfBoundsException {
        if (model.offer(n) == true)
            view.setMsgLabel("Added: " + n);
        else
            view.setMsgLabel("Queue is full.");

    }

    public void insert(int n, int nn) throws IndexOutOfBoundsException {
        if (model.offer(n, nn) == true)
            view.setMsgLabel("Added: " + n + " (Priority: " + nn + " )");
        else
            view.setMsgLabel("Queue is full.");

    }

    public void remove() {
        Integer top = model.poll();
        if (top != null) {
            view.setMsgLabel("Removed: " + top.toString());
        } else {
            view.setMsgLabel("Queue is empty.");
        }
    }


    public TreeView<Integer> getTree() {
        return model.toTreeView();
    }

    public void setView(ConsoleView view) {
        this.view = view;
    }


    public void sort() {
        model.sort();
    }
}
