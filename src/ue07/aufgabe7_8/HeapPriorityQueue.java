package ue07.aufgabe7_8;

import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

import java.util.AbstractQueue;
import java.util.Arrays;
import java.util.Iterator;
import java.util.PriorityQueue;

public class HeapPriorityQueue<E extends Comparable<E>> extends AbstractQueue<E> {

    private int queueCapacity; // maximale Groesse
    private int queueSize = 0; // aktuelle Groesse

    private Integer[] priority;
    private E[] queueArray;

    /**
     * @param capacity die Kapazitaet der Warteschlange.
     * @throws IllegalArgumentException falls queueCapacity <= 0
     */
    @SuppressWarnings("unchecked")
    public HeapPriorityQueue(int capacity) throws IllegalArgumentException {
        if (capacity <= 0) {
            throw new IllegalArgumentException();
        }
        this.queueCapacity = capacity;
        this.queueSize = 0;
        this.queueArray = (E[]) new Comparable[capacity];
        this.priority = new Integer[capacity];
    }

    /**
     * @param e the element to add
     * @return {@code true} if element is added, {@code false} if queue full.
     * @throws ClassCastException
     * @throws NullPointerException
     */
    @Override
    public boolean offer(E e) throws NullPointerException {
        if (e == null)
            throw new NullPointerException();
        if (queueSize < queueArray.length) {
            queueArray[queueSize] = e;
            priority[queueSize] = queueSize;
            queueSize++;
            return true;
        } else {
            return false;
        }
    }

    public boolean offer(E e, int prio) throws NullPointerException {
        if (e == null)
            throw new NullPointerException();
        if (queueSize < queueArray.length) {
            queueArray[queueSize] = e;
            priority[queueSize] = prio;
            queueSize++;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the head of the Priotiylists and removes it.
     *
     * @return {@code null} if queue empty, {@code result} for the head of the old prioritylist
     */
    @Override
    public E poll() {
        if (queueSize == 0)
            return null;
        queueSize--;
        Integer newprio[] = new Integer[queueCapacity];
        E result = queueArray[0];
        E[] qA = (E[]) new Comparable[queueCapacity];
        if (queueSize != 0) {
            for (int i = 0; i < queueArray.length; i++) {
                if (i < queueCapacity - 1) {
                    qA[i] = queueArray[i + 1];
                    newprio[i] = priority[i + 1];
                }
            }
        }
        queueArray = qA;
        priority = newprio;
        return result;
    }


    /**
     * Returns the head of the queue
     *
     * @return Head of Queue
     */
    @Override
    public E peek() {
        if (queueSize == 0) return null;
        else return queueArray[0];
    }

    /**
     * @return the Iterator of the Queue
     */
    @Override
    public Iterator<E> iterator() {
        return Arrays.asList(Arrays.copyOf(queueArray, queueSize)).iterator();
    }

    /**
     * the current size of the queue
     *
     * @return size
     */
    @Override
    public int size() {
        return this.queueSize;
    }

    /**
     * Builds up a TreeView version of the Queue.
     *
     * @return TreeView of Queue
     */

    public TreeView<E> toTreeView() {
        TreeView<E> treeView = new TreeView<>();
        if (queueSize > 0) {
            TreeItem<E> root = new TreeItem<E>((E) "Queue:");
            root.setExpanded(true);

            for (int i = 0; i < queueSize; i++) {
                TreeItem<E> item = new TreeItem<E>(queueArray[i]);
                TreeItem<E> priohead = new TreeItem<E>((E) "Priority: ");
                TreeItem<E> prio = new TreeItem<>((E) priority[i]);
                priohead.setExpanded(true);
                root.getChildren().add(item);
                item.getChildren().add(priohead);
                priohead.getChildren().add(prio);
            }
            treeView.setRoot(root);
            return treeView;
        } else {
            return treeView;
        }

    }

    public void sort() {
        for (int i = 0; i < queueSize; i++) {
            for (int j = 1; j < queueSize; j++) {
                if (priority[j - 1] > priority[j]) {
                    E tmp = queueArray[j];
                    queueArray[j] = queueArray[j - 1];
                    queueArray[j - 1] = tmp;
                    Integer tmp1 = priority[j];
                    priority[j] = priority[j - 1];
                    priority[j - 1] = tmp1;
                }
            }
        }
    }
}
