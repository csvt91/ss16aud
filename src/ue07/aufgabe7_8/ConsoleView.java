package ue07.aufgabe7_8;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

class FormattedTextField extends TextField {

    public static String NumberPat = "[0-9]";
    public static String TextPat = "[a-zA-Z]";

    private String format;
    private int size;

    public FormattedTextField(String format, int... size) {
        this.format = format;
        this.size = 0;
        if (size.length == 1 && size[0] > 1) {
            this.size = size[0];
            super.setPrefColumnCount(size[0]);
        }
    }

    @Override
    public void replaceText(int start, int end, String text) {
        if (text.matches(format) || text == "") {
            if (size <= 0 || getText().length() < size) {
                super.replaceText(start, end, text);
            }
        }
    }

    @Override
    public void replaceSelection(String text) {
        if (text.matches(format) || text == "") {
            if (size <= 0 || getText().length() < size) {
                super.replaceSelection(text);
            }
        }
    }
}


public class ConsoleView extends GridPane {
    private Control control;

    private Button insertButton = new Button("Insert");
    private TextField insertTextfield = new FormattedTextField(FormattedTextField.NumberPat, 15);
    private TextField insertPriorityfield = new FormattedTextField(FormattedTextField.NumberPat, 5);
    private Button removeButton = new Button("Remove");
    private Button sortButton = new Button("Sort");
    private Label valueLabel = new Label("");
    private Label msgLabel = new Label("");

    private Stage treeStage = null;

    public ConsoleView(Control control) {
        this.control = control;
        setPadding(new Insets(10, 10, 10, 10));
        setHgap(5);
        setVgap(5);

        insertPriorityfield.setText("Priority");
        insertTextfield.setText("Number to add in the Queue");
        insertPriorityfield.setEditable(true);
        add(insertTextfield, 0, 0);
        add(insertPriorityfield, 1, 0);
        add(insertButton, 0, 1);
        add(removeButton, 0, 1);
        add(sortButton, 0, 1);
        add(valueLabel, 1, 1);
        add(msgLabel, 1, 2);
        setHalignment(insertButton, HPos.LEFT);
        setHalignment(sortButton, HPos.CENTER);
        setHalignment(removeButton, HPos.RIGHT);
        setHalignment(msgLabel, HPos.LEFT);
        insertPriorityfield.setOnMousePressed(
                (event) -> {
                    insertPriorityfield.setText("");
                    insertPriorityfield.setEditable(true);
                }
        );
        insertTextfield.setOnMousePressed(
                (event) -> {
                    insertTextfield.setText("");
                    insertTextfield.setEditable(true);
                }
        );
        insertButton.setOnAction(
                (event) -> {
                    msgLabel.setText("");
                    String nS = insertTextfield.getText();
                    String nSs = insertPriorityfield.getText();
                    try {
                        int n = Integer.parseInt(nS);
                        if (!nSs.equals("Priority")) {
                            int nN = Integer.parseInt(nSs);
                            control.insert(n, nN);
                        } else
                            control.insert(n);
                        TreeView<Integer> treeView = this.control.getTree();
                        Scene secondScene = new Scene(treeView);
                        if (treeStage != null) {
                            treeStage.close();
                            treeStage = null;
                        }
                        treeStage = new Stage();
                        treeStage.setTitle("Heap");
                        treeStage.setScene(secondScene);
                        treeStage.show();
                    } catch (NumberFormatException e) {
                        msgLabel.setText("Illegal value to be inserted");
                    }
                }
        );
        removeButton.setOnAction(
                (event) -> {
                    msgLabel.setText("");
                    control.remove();
                    TreeView<Integer> treeView = this.control.getTree();
                    Scene secondScene = new Scene(treeView);
                    if (treeStage != null) {
                        treeStage.close();
                        treeStage = null;
                    }
                    treeStage = new Stage();
                    treeStage.setTitle("Heap");
                    treeStage.setScene(secondScene);
                    treeStage.show();
                }
        );
        sortButton.setOnAction(
                (event) -> {
                    msgLabel.setText("");
                    control.sort();
                    TreeView<Integer> treeView = this.control.getTree();
                    Scene secondScene = new Scene(treeView);
                    if (treeStage != null) {
                        treeStage.close();
                        treeStage = null;
                    }
                    treeStage = new Stage();
                    treeStage.setTitle("Heap");
                    treeStage.setScene(secondScene);
                    treeStage.show();
                }
        );


    }

    public void setValueLabel(String msg) {
        valueLabel.setText(msg);
    }

    public void setMsgLabel(String msg) {
        msgLabel.setText(msg);
    }

}
