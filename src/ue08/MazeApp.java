package ue08;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Created by Caroline Vetter on 02.06.2016.
 */
public class MazeApp extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
    /*    primaryStage.setTitle("MazeOLD App");

        MazeOLD m = new MazeOLD();
        Controller control = new Controller(m);
        View controlView = new View(control);

        control.setView(controlView);

        FlowPane root = new FlowPane();
        root.getChildren().add(controlView);

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();*/
    }

    public static void main(String args[]) {
        MazeOLD m = new MazeOLD();
        m.setNewMaze(new Integer[]{5, 6});
        m.fill(3);
        m.remove(3);
        m.empty();
        m.fill(4);
        System.out.print(m.print());
    }

}
