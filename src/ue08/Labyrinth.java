package ue08;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Caroline Vetter (977447)
 * @version 0 @06.06.2016.
 */
public class Labyrinth {
    private int[] size;
    private Tile board[][] = null;
    private int walker = 1;
    private Tile current = null;

    public Labyrinth(int x, int y) {
        this.size[0] = x;
        this.size[1] = y;
        fillWithTiles();
    }

    private void fillWithTiles() {
        this.board = new Tile[size[0]][size[1]];
        for (int i = 0; i < size[0]; i++)
            for (int j = 0; j < size[1]; j++)
                board[i][j] = new Tile(this, i, j);
        current = board[0][0];
    }

    public Tile getTile(int x, int y) {
        return board[x][y];
    }

    public List<Tile> solveLabyrinth() {
        List<Tile> route = new ArrayList<>();
        while (!current.isEndPoint()) {
            List<Tile> tiles = current.theseus();
            Iterator<Tile> tileIta = tiles.iterator();
            while (tileIta.hasNext()) {
                Tile temp = tileIta.next();
                if (!temp.gotVisited()) {
//TODO
                }
            }
        }
        return route;
    }


}
