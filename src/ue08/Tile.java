package ue08;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Caroline Vetter (977447)
 * @version 0 @06.06.2016.
 */
public class Tile {
    private int[] borderlines;
    private int x, y;
    private boolean visited;
    private Labyrinth master;
    private boolean isEnd;
    private int pointflag;

    public Tile(Labyrinth laby, int x, int y) {
        this.master = laby;
        this.x = x;
        this.y = y;
        this.visited = false;
    }

    public Tile(int x, int y, int[] borderlines, boolean visited) {
        this.x = x;
        this.y = y;
        this.borderlines = borderlines;
        this.visited = visited;
    }


    public void setBorders(int direction) {
        borderlines[direction] = 1;
    }

    public void removeBorders(int direction) {
        borderlines[direction] = 0;
    }

    public List<Tile> theseus() {
        List<Tile> result = new ArrayList<>();
        int col = x;
        int row = y;
        if (this.borderlines[0] == 0 && x - 1 > 0)
            result.add(checkBorder(row, col, 0));
        if (this.borderlines[1] == 0 && y - 1 >= 0)
            result.add(checkBorder(row, col, 1));
        if (this.borderlines[1] == 0 && y - 1 >= 0)
            result.add(checkBorder(row, col, 2));
        if (this.borderlines[1] == 0 && y - 1 >= 0)
            result.add(checkBorder(row, col, 3));

        return result;
    }

    private Tile checkBorder(int x, int y, int side) {
        if (side == 0)
            return new Tile(x - 1, y, master.getTile(x - 1, y).getBorderlines(), master.getTile(x - 1, y).gotVisited());
        if (side == 1)
            return new Tile(x, y - 1, master.getTile(x, y - 1).getBorderlines(), master.getTile(x - 1, y).gotVisited());
        if (side == 2)
            return new Tile(x + 1, y, master.getTile(x + 1, y).getBorderlines(), master.getTile(x - 1, y).gotVisited());
        if (side == 3)
            return new Tile(x, y + 1, master.getTile(x, y + 1).getBorderlines(), master.getTile(x - 1, y).gotVisited());
        return null;
    }

    public int[] getBorderlines() {
        return borderlines;
    }

    public boolean gotVisited() {
        return visited;
    }

    public String toString() {
        return "X={" + x + "} Y={" + y + "}";
    }

    public boolean isEndPoint() {
        return isEnd;
    }
}
