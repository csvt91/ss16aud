package ue08;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class MazeOLD {
    private Integer[] mazeBoard;
    private Integer[] size; // x & y

    /**
     * 0 = EMPTY (free way)
     * 1 = BLOCKED
     * 2 = current Route Search
     */
    MazeOLD() {

    }

    /**
     * generates a new mazeBoard and saves the Size for later usage
     *
     * @param size x and y of the Board
     */
    void setNewMaze(Integer[] size) {
        this.size = size;
        this.mazeBoard = fillEmpty(new Integer[size[0]*size[1]]);

    }

    private Integer[] fillEmpty(Integer[] array) {
        for (int i = 0; i < array.length; i++)
            array[i] = 0;

        return array;
    }

    public Integer[] getMazeBoard() {
        return mazeBoard;
    }

    public void setMazeBoard(Integer[] mazeBoard) {
        this.mazeBoard = mazeBoard;
    }

    public void fill(int i) {
        if (inBoard(i)) {
            this.mazeBoard[i] = 1;
        }
    }

    public void remove(int i) {
        if (inBoard(i)) {
            this.mazeBoard[i] = 0;
        }
    }

    public void empty() {
        mazeBoard = fillEmpty(new Integer[size[0] * size[1]]);
    }

    private boolean inBoard(int i) {
        return (i < this.mazeBoard.length && i >= 0) ?
                true : false;

    }

    public String print() {
        List<String> arrs = new ArrayList<>();
        for (int i = 0; i < mazeBoard.length; i = i + size[0]) {
            arrs.add("" + Arrays.toString(Arrays.copyOfRange(mazeBoard, i, i + size[0])));
        }
        String result = "";
        for (String s : arrs)
            result += "" + s + "\n";
        return result;
    }

    List<Integer> route = new ArrayList<>();

    /**
     * Searches for Routes.
     *
     * @param currentmaze
     * @param current
     * @return
     */
    public boolean findRoute(Integer[] currentmaze, int current) {
        if (isEmpty(currentmaze)) return false;
        List<Integer> possibleRoutes = checkForRoutes(currentmaze, current);


        findRoute(currentmaze, current);
        return false;
    }

    /**
     * Checks for possible routes up, down, left and right.
     *
     * @param currentmaze the currentmaze
     * @param current     the current index
     * @return the result of the route search
     */
    private List<Integer> checkForRoutes(Integer[] currentmaze, int current) {
        // right, left, down, up
        Integer[] ways = {1, -1, size[0] + current + 1, size[0] - current - 1};
        List<Integer> routes = new ArrayList<>();
        for (Integer way : ways) {
            if (currentmaze[current + way] == 0)
                routes.add(current + way);

        }
        return null;

    }

    public boolean isEmpty(Integer[] currentmaze) {
        int count = 0;
        for (int i : currentmaze)
            if (i == 0)
                count++;
        return (count == 0) ? true : false;
    }
}