package ue08;

/**
 * Created by User on 02.06.2016.
 */
public class Controller {
    private View view;
    private MazeOLD m;
    private int width, height;

    public Controller(MazeOLD m) {
        this.m = m;
    }

    public void setView(View view) {
        this.view = view;
    }

    public Integer[] getXY() {
        return new Integer[]{width, height};
    }
}
