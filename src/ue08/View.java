package ue08;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

class View extends GridPane {
    private Controller control;
    private Button[] playField;
    private Label timer = new Label();

    View(Controller control) {
        this.control = control;
        Integer[] size = this.control.getXY();
        setWidth(size[0] * 75 + 100);
        setHeight(size[1] * 75);
        setMaxSize(size[0] * 75 + 100, size[1] * 75);

    }


}
