package carofuns.mypriorityqueue;

import java.util.AbstractQueue;
import java.util.Iterator;

public class MyPriorityQueue<E extends Comparable<E>> extends AbstractQueue<E> {

    private E[] store;
    private int capacity; // maximale Groesse
    private int size = 0; // aktuelle Groesse

    /**
     * Erzeugt eine Prioritaetswarteschlange.
     *
     * @param capacity die Kapazitaet der Warteschlange.
     * @throws IllegalArgumentException falls capacity <= 0
     */

    @SuppressWarnings("unchecked")
    public MyPriorityQueue(int capacity) throws IllegalArgumentException {
        if (capacity <= 0) {
            throw new IllegalArgumentException();
        }
        store = (E[]) new Comparable[capacity];
        this.capacity = capacity;
    }

    @Override
    public boolean offer(E e) {
        if (e == null)
            throw new NullPointerException();
        if (size < capacity) {
            this.store[size] = e;
            insertionSort();
            size++;
            return true;
        } else
            return false;
    }

    @Override
    public E poll() {
        if (store[0] != null) {
            E ersterWert = store[0];

            for (int i = 0; i < size - 1; i++) {
                store[i] = store[i + 1];
            }
            store[--size] = null;
            return ersterWert;
        } else
            return null;
    }

    @Override
    public E peek() {
        if (store[0] == null)
            return null;
        return (E) store[0];
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {

            @Override
            public boolean hasNext() {
                if (next() == null)
                    return false;
                return true;
            }

            @Override
            public E next() {

                if (size + 1 >= capacity)
                    return null;
                return store[size + 1];

            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();

            }

        };
    }

    @Override
    public int size() {
        return this.size;

    }

    public void insertionSort() {
        for (int j = 1; j <= size; j++) {
            E key = store[j];
            int i = j;
            while (i > 0 && store[i - 1].compareTo(key) > 0) {
                store[i] = store[i - 1];
                i--;
            }
            store[i] = key;
        }

    }

}
