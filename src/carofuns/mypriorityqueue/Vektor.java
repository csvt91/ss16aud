package carofuns.mypriorityqueue;

public class Vektor  implements Comparable <Vektor> {
	
	private int x;
	private int y;
	
	public Vektor(int x, int y) {
	
		this.x = x;
		this.y = y;
	}

	public double lenght(){
		return Math.sqrt(x*x+y*y);
	}

	@Override
	public String toString() {
		return "Vektor [x=" + x + ", y=" + y + "]";
	}
	
	@Override
	public int compareTo(Vektor that) {
		
		return (int)((this.lenght() - that.lenght())*10000);
	}
	
	

}
