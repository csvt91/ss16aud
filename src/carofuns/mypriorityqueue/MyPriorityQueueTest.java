package carofuns.mypriorityqueue;

import static org.junit.Assert.*;

import org.junit.Test;

public class MyPriorityQueueTest {

	@SuppressWarnings("unused")
	@Test
	public void test() {
		MyPriorityQueue<String> q = new MyPriorityQueue<>(10);
		q.offer("8");
		q.offer("2");
		q.offer("4");
		q.offer("5");
		q.offer("7");
		q.offer("1");
		q.offer("3");
		q.offer("6");
		q.offer("9");

		String[] a = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		int i = 0;
		for (String s : a) {
			assertEquals(a[i++], q.poll());
		}

		assertEquals(true, q.offer("9"));
		assertEquals(true, q.offer("8"));
		assertEquals(true, q.offer("2"));
		assertEquals(true, q.offer("4"));
		assertEquals(true, q.offer("5"));
		assertEquals(true, q.offer("7"));
		assertEquals(true, q.offer("1"));
		assertEquals(true, q.offer("3"));
		assertEquals(true, q.offer("6"));
		assertEquals(true, q.offer("9"));
		assertEquals(false, q.offer("11"));
		assertEquals(false, q.offer("12"));
		a = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "9" };
		i = 0;
		for (String s : q) {
			System.out.println(i);
			assertEquals(a[i++], s);
		}

	}

}
