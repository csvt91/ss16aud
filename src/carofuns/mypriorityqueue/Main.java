package carofuns.mypriorityqueue;


import java.util.PriorityQueue;
import java.util.Queue;

public class Main<E> extends PriorityQueue<E> {


    /**
     *
     */
    private static final long serialVersionUID = -8005004401190678886L;


    /**
     * @author Nils Leistner, 967790
     */

    public static void main(String[] args) {

        Vektor A[] = {new Vektor(1, 2), new Vektor(1, 1), new Vektor(2, 2),
                new Vektor(2, 1), new Vektor(0, 1), new Vektor(0, 2),
                new Vektor(0, 1), new Vektor(0, 2), new Vektor(0, 0),
                new Vektor(0, 0)};

        MyPriorityQueue<Vektor> q = new MyPriorityQueue<>(10);
        sortQ(A, q);
        for (Vektor v : A) {
            System.out.println(v);
        }
    }


    private static <T extends Comparable<T>> void sortQ(T[] a, Queue<T> q) {
        for (T e : a) {
            q.offer(e);
        }

        for (int i = 0; i < a.length; i++) {
            a[i] = q.poll();
        }


    }


}
