package carofuns;

/**
 * @author Caroline Vetter
 * @version 0.1 @ 21.04.2016.
 */
public class Funk<T extends Comparable<T>> {
    int x;
    int y;
    T[] a;

    public Funk(int a, int v) {
        this.x = a;
        this.y = v;
    }

    public Funk(int a) {
        this.x = a;
    }

    public Funk() {
        // for fib etc
    }

    public Funk(T[] t) {
        this.a = t;
    }

    /**
     * @return größterer gemeinsamer Teiler
     */
    public int ggt() {
        if (x <= 0 || y <= 0) {
            throw new IllegalArgumentException("Beide Zahlen müssen >0 sein.");
        }
        int erg = x < y ? x : y;
        while (x % erg != 0 || y % erg != 0) {
            erg--;
        }
        return erg;
    }

    /**
     * EUCLID_OLD(a,b)
     * GGT(m; n)
     * while m != n
     * r = m mod n
     * m = n
     * n = r
     * return r
     *
     * @return größter gemeinsamer Teiler
     */
    public int ggtEuk() {
        if (y <= 0 || x <= 0) {
            throw new IllegalArgumentException("Beide Zahlen müssen >0 sein.");
        }
        int r;
        while (y != x && y != 0) {
            r = x % y;
            x = y;
            y = r;
        }
        return x;
    }

    public int ggtRe(int a, int b) {
        if (a <= 0 || b <= 0)
            throw new IllegalArgumentException("Beide Zahlen müssen >0 sein.");
        if (a == b) return (a);
        else {
            return (a > b) ? (ggtRe(a - b, b)) : (ggtRe(b - a, a));
        }
    }


    /**
     * Fibonacci
     *
     * @return Zahl der Fibonacci Folge an qter Stelle
     */
    public int fib(int q) {
        if (q < 0) {
            throw new IllegalArgumentException("Fibonacci Rechnung für negative Zahlen ist nicht definiert.");
        } else if (q <= 0) {
            return 0;
        } else if (q == 1) {
            return 1;
        } else {
            return fib(q - 2) + fib(q - 1);
        }
    }

    /**
     * Bubblesort
     *
     * @return sorted Array
     */
    public T[] bubbleSort() {
        for (int i = 0; i < a.length; i++) {
            for (int j = 1; j < a.length; j++) {
                if (a[j - 1].compareTo(a[j]) > 0) {
                    T tmp = a[j];
                    a[j] = a[j - 1];
                    a[j - 1] = tmp;
                }
            }
        }
        return a;
    }

    /**
     * Insertion Sort
     */
    public T[] insertionSort() {
        for (int j = 1; j <= a.length; j++) {
            T key = a[j];
            int i = j;
            while (i > 0 && a[i - 1].compareTo(key) > 0) {
                a[i] = a[i - 1];
                i--;
            }
            a[i] = key;
        }
        return a;
    }

    public String ArrayToString() {
        String s = "";
        for (T b : a)
            s += b + " ";
        return s;
    }

    public String ArrayToString(T[] arr) {
        String s = "";
        for (T b : arr)
            s += b + " ";
        return s;
    }
}
