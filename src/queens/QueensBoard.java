package queens;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.IntStream;

/**
 * Instances of this class represent boards of size NxN partially filled with
 * queens.
 * Each instance has at most one queen in a row. A board is not necessarily safe.
 */
public class QueensBoard {
    private boolean[][] board;
    private int N;
    private int nextRowToFill = 0;

    public QueensBoard(int N) {
        this.N = N;
        board = new boolean[N][N];
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        IntStream.range(0, N).forEach(i -> {
            IntStream.range(0, N).forEach(j -> str.append(board[i][j] ? "X " : "- "));
            str.append("\n");
        });
        return str.toString();
    }

    /**
     * Checks whether this board with its placement of queens is safe.
     *
     * @return true if all queens on this board are safe from each other.
     */
    public boolean acceptable() {
        for (int i = 0; i < nextRowToFill; i++) {
// x <- placement of queen in row i
            int ii = i;
            int x = IntStream.range(0, N).filter(c -> board[ii][c]).findFirst().getAsInt();
            for (int j = 0; j < nextRowToFill; j++) {
                if (i != j) {
                    int d = i - j;
// diagonal: distance between j an j
// x <- placement of queen in row j
                    int jj = j;
                    int y = IntStream.range(0, N).filter(c ->
                            board[jj][c]).findFirst().getAsInt();
                    if (y == x || y == x - d || y == x + d) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Compute all boards with one queen more. These boards are not necessarily
     * <p>
     * safe.
     *
     * @return an iterable that iterates over all boards with one queen more.
     */
    public Iterable<QueensBoard> directSuccessors() {
        final int actualRow = nextRowToFill;
        if (actualRow == N) {
            return new Iterable<QueensBoard>() {
                @Override
                public Iterator<QueensBoard> iterator() {
                    return new Iterator<QueensBoard>() {
                        @Override
                        public boolean hasNext() {
                            return false;
                        }

                        @Override
                        public QueensBoard next() {
                            throw new NoSuchElementException();
                        }
                    };
                }
            };
        } else {
            return new Iterable<QueensBoard>() {
                int column = 0;

                @Override
                public Iterator<QueensBoard> iterator() {
                    return new Iterator<QueensBoard>() {
                        @Override
                        public boolean hasNext() {
                            return column != N;
                        }

                        @Override
                        public QueensBoard next() {
                            QueensBoard nextBoard = new QueensBoard(N);
                            nextBoard.board = new boolean[N][N];
                            for (int i = 0; i < N; i++) {
                                nextBoard.board[i] = Arrays.copyOf(QueensBoard.this.board[i], N);
                            }
                            nextBoard.board[actualRow][column] = true;
                            nextBoard.nextRowToFill = QueensBoard.this.nextRowToFill + 1;
                            return nextBoard;
                        }
                    };
                }
            };
        }
    }

    public boolean isComplete() {
        return nextRowToFill == N;
    }
}
