package ue01.aufgabe4;

/**
 * @author Caroline Vetter (977447)
 * @version 0 @23.05.2016.
 */
final class Menge {
    private int[] m;

    public Menge(int... a) {
        m = new int[a.length];
        int i = 0;
        for (int x : a) {
            m[i++] = x;
        }
    }

    public boolean enthaelt(int x) {
        for (int e : m) {
            if (x == e) return true;
        }
        return false;
    }

    public void fuegeEin(int x) {
        if (!enthaelt(x)) {
            int[] t = new int[m.length + 1];
            int i = 0;
            for (int y : m) {
                t[i++] = y;
            }
            t[i++] = x;
            m = t;
        }
    }

    public void entferne(int x) {
        if (enthaelt(x)) {
            int ix = -1;
            for (int j = 0; j <= m.length; j++) {
                if (m[j] == x) {
                    ix = j;
                    break;
                }
            }
            int[] t = new int[m.length - 1];
            int i = 0;
            for (int y : m) {
                if (i != ix) {
                    t[i++] = y;
                }
            }
            m = t;
        }
    }

    public Menge vereinigtMit(Menge s) {
        int count = this.m.length;
        for (int x : s.m) {
            if (!this.enthaelt(x)) {
                count++;
            }
        }
        int[] t = new int[count];
        int i = 0;
        for (int x : this.m) {
            t[i++] = x;
        }
        for (int x : s.m) {
            if (!this.enthaelt(x)) {
                t[i++] = x;
            }
        }
        return new Menge(t);
    }

    public Menge geschnittenMit(Menge s) {
        int count = 0;
        for (int x : this.m) {
            if (s.enthaelt(x)) {
                count++;
            }
        }
        int[] t = new int[count];
        int i = 0;
        for (int x : this.m) {
            if (s.enthaelt(x)) {
                t[i++] = x;
            }
        }
        return new Menge(t);
    }

    public boolean teilMengeVon(Menge that) {
        if (this.m.length > that.m.length) {
            return false;
        }
        for (int x : this.m) {
            if (!that.enthaelt(x)) {
                return false;
            }
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        for (int i : m) {
            sb.append("" + i + (i < m.length - 1 ? ", " : ""));
        }
        return sb.toString();
    }

    public boolean equals(Object other) {
        if (!(other instanceof Menge)) {
            return false;
        } else {
            Menge that = (Menge) other;
            if (m.length != that.m.length) {
                return false;
            }
            for (int x : this.m) {
                if (!that.enthaelt(x)) {
                    return false;
                }
            }
            return true;
        }
    }
}