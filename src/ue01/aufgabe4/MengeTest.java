package ue01.aufgabe4;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class MengeTest {
    @Test
    public void test() {
        assertEquals(new Menge(3, 4, 5, 6),
                new Menge(3, 4).vereinigtMit(new Menge(5, 6)));
        assertEquals(new Menge(3, 6),
                new Menge(3, 4, 5, 6).geschnittenMit(new Menge(3, 6)));
        assertEquals(new Menge(),
                new Menge(3, 4, 5, 6).geschnittenMit(new Menge(7, 8, 9, 10)));
        assertTrue("ERROR",
                new Menge(3, 6).teilMengeVon(new Menge(3, 4, 5, 6)));
        assertTrue("ERROR",
                new Menge(3, 4, 5, 6).enthaelt(4));
        assertTrue("ERROR",
                new Menge(3, 4, 5, 6).equals(new Menge(4, 6, 5, 3)));
    }


}