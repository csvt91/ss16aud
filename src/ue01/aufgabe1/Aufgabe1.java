package ue01.aufgabe1;

/**
 * @author Caroline Vetter (977447)
 * @version 0 @23.05.2016.
 */
class Calculus {
    interface Function {
        double apply(double x);
    }

    static double integral(Function f, double a, double b) {
        final int N = 100;
        double sum = 0.0;
        double delta = (b - a) / N;
        for (int i = 0; i < N; i++) {
            sum = sum + delta * f.apply(a + i * delta);
        }
        return sum;
    }
}

public class Aufgabe1 {
    public static void main(String[] args) {
        double area = Calculus.integral(
                new Calculus.Function() {
                    @Override
                    public double apply(double x) {
                        return Math.sqrt(x);
                    }
                },
                0.0, 1.0);
        System.out.println("Berechnet:" + area);
        System.out.println("zum Vergleich exakt:" + 2.0 / 3.0);
    }
}