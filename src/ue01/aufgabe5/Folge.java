package ue01.aufgabe5;

/**
 * @author Caroline Vetter (977447)
 * @version 0 @23.05.2016.
 */
interface Folge {
    public double glied(int n);
}

class FolgeA implements Folge {
    // Teilaufgabe 5.1
    private double x;
    private double c;
    private double s;

    public FolgeA(double s, double x, double c) {
        this.s = s;
        this.x = x;
        this.c = c;
    }

    public double glied(int n) {
        if (n == 0) {
            return s;
        } else {
            return glied(n - 1)
                    *
                    x + c;
        }
    }

    @Override
    public boolean equals(Object that) {
        if (!(that instanceof FolgeA)) {
            return false;
        } else {
            FolgeA other = (FolgeA) that;
            return Double.compare(this.x, other.x) == 0
                    && Double.compare(this.c, other.c) == 0
                    && Double.compare(this.s, other.s) == 0;
        }
    }

    @Override
    public String toString() {
        return "FolgeA [" + s + ", " + x + ", " + c + "]";
    }
}