package ue01.aufgabe2;

/**
 * @author Caroline Vetter (977447)
 * @version 0 @23.05.2016.
 */

import java.util.function.Function;

class Calculus_Java8 {
    static double integral(Function<Double, Double> f, double a, double b) {
        final int N = 100;
        double sum = 0.0;
        double delta = (b - a) / N;
        for (int i = 0; i < N; i++) {
            sum = sum + delta * f.apply(a + i * delta);
        }
        return sum;
    }
}

public class Aufgabe2 {
    public static void main(String[] args) {
        double area = Calculus_Java8.integral(
                x -> Math.sqrt(x),
                0.0, 1.0);
        System.out.println("Berechnet:" + area);
        System.out.println("zum Vergleich exakt:" + 2.0 / 3.0);
    }
}
