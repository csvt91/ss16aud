package ue01.aufgabe3;

/**
 * @author Caroline Vetter (977447)
 * @version 0 @23.05.2016.
 */
class Poly {
    private double[] a;

    public Poly(double... a) {
        this.a = new double[a.length];
        int i = 0;
        for (double a_i : a) {
            this.a[i++] = a_i;
        }
    }

    public double valueAt(double x) {
        double val = 0.0;
        for (double a_i : a) {
            val = a_i + val
                    *
                    x;
        }
        return val;
    }
}

public class Aufgabe3 {
    public static void main(String[] args) {
        Poly p = new Poly(1, 2, 3);
        System.out.println(p.valueAt(1.0));
    }
}