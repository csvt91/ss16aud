package loopy;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class Loops {

    public static void main(String[] args) {
        List<Integer> lst = Arrays.asList(new Integer[]{3, 4, 5, 6, 7, 8, 9});
        System.out.println(sum1(lst));
        System.out.println(sum2(lst));
        System.out.println(sum3(lst));
        System.out.println(sum4(lst));
        System.out.println(sum5(lst));
        System.out.println(new Sum6().apply(lst));
    }

    private static int sum1(List<Integer> lst) {
        int result = 0;
        for (int i = 0; i < lst.size(); i++) {
            result += lst.get(i);
        }
        return result;
    }

    private static int sum2(List<Integer> lst) {
        int result = 0;
        for (int i = 0; i < lst.size(); i++) {
            result += lst.get(i);
        }
        return result;
    }

    private static int sum3(List<Integer> lst) {
        int result = 0;
        for (int e : lst) {
            result += e;
        }
        return result;
    }

    private static int sum4(Iterable<Integer> lst) {
        int result = 0;
        for (int e : lst) {
            result += e;
        }
        return result;
    }

    private static int sum5(List<Integer> lst) {
        return lst.stream().reduce(0, (a, x) -> a + x);
    }

    private static class Sum6 implements Function<Iterable<Integer>, Integer> {
        int result;

        public Integer apply(Iterable<Integer> lst) {
            result = 0;
            lst.iterator().forEachRemaining(e -> {
                result += e;
            });
            return result;
        }
    }
}